//
//  EnUygunTests.swift
//  EnUygunTests
//
//  Created by Taha Buyruk on 10.05.2018.
//  Copyright © 2018 Taha Buyruk. All rights reserved.
//

import XCTest
@testable import EnUygun

class EnUygunTests: XCTestCase {
   
    var gidisUcagiControllerUnderTest: UcuslarController!
    
    let expectedDate = "11.01"
    let expectedTime = "23"
  
    let expectedAirlineName = "THY"
    let expectedAirlineCode = "TK"
    
    let expectedAirportName = "ESB"
    let expectedAirportCode = "ES"
    
    let expectedCarryOnAllowance = 8
    let expectedCarryOnUnit = "Kg"
    
    let expectedDepartureDateTimeDate = "12.08"
    let expectedDepartureDateTimeTime = "23.58"
    
    let expectedDay = 1
    let expectedHour = 2
    let expectedMinute = 32
    let expectedTotalMinutes = 1586
    
    let expectedAllowance = 1
    let expectedPaxType = "deneme"
    let expectedUnit = "KG"
    
    var systemUnderArrivalDateTimeTest: ArrivalDateTime!
    var systemUnderAirlineTest: Airline!
    var systemUnderAirportTest: Airport!
    var systemUnderCarryOnTest: CarryOn!
   var systemUnderDepartureDateTimeTest: DepartureDateTime!
     var systemUnderDurationTest: Duration!
    var systemUnderFirstBaggageCollection: FirstBaggageCollection!
    
    override func setUp() {
        super.setUp()
        systemUnderArrivalDateTimeTest = ArrivalDateTime(date: expectedDate, time: expectedTime)
        systemUnderAirlineTest = Airline(code: expectedAirlineCode, name: expectedAirlineName)
        systemUnderAirportTest = Airport(code: expectedAirportCode, name: expectedAirportName)
        systemUnderCarryOnTest = CarryOn(allowance: expectedCarryOnAllowance, unit: expectedCarryOnUnit)
        systemUnderDepartureDateTimeTest = DepartureDateTime(date: expectedDepartureDateTimeDate, time: expectedDepartureDateTimeTime)
      systemUnderDurationTest = Duration(day: expectedDay, hour: expectedHour, minute: expectedMinute, totalMinutes: expectedTotalMinutes)
        systemUnderFirstBaggageCollection = FirstBaggageCollection(allowance: expectedAllowance, paxType: expectedPaxType, unit: expectedUnit)
    }
    func testSUT_InitializessystemUnderFirstBaggageCollectionUnit() {
        XCTAssertEqual(systemUnderFirstBaggageCollection.unit, expectedUnit)
    }
    func testSUT_InitializessystemUnderFirstBaggageCollectionPaxType() {
        XCTAssertEqual(systemUnderFirstBaggageCollection.paxType, expectedPaxType)
    }
    func testSUT_InitializessystemUnderFirstBaggageCollectionAllowance() {
        XCTAssertEqual(systemUnderFirstBaggageCollection.allowance, expectedAllowance)
    }
    
    
    
    
    
    
    
    
    func testSUT_InitializesDurationDay() {
        XCTAssertEqual(systemUnderDurationTest.day, expectedDay)
    }
    
    func testSUT_InitializesDurationHour() {
        XCTAssertEqual(systemUnderDurationTest.hour, expectedHour)
    }
    func testSUT_InitializesDurationMinute() {
        XCTAssertEqual(systemUnderDurationTest.minute, expectedMinute)
    }
    
    func testSUT_InitializesDurationTotalminutes() {
        XCTAssertEqual(systemUnderDurationTest.totalMinutes, expectedTotalMinutes)
    }
    
    func testSUT_InitializesDepartureDateTimeDate() {
        XCTAssertEqual(systemUnderDepartureDateTimeTest.date, expectedDepartureDateTimeDate)
    }
    
    func testSUT_InitializesDepartureDateTimeTime() {
        XCTAssertEqual(systemUnderDepartureDateTimeTest.time, expectedDepartureDateTimeTime)
    }
    func testSUT_InitializesCarryOnAllowance() {
        XCTAssertEqual(systemUnderCarryOnTest.allowance, expectedCarryOnAllowance)
    }
    
    func testSUT_InitializesAirportName() {
        XCTAssertEqual(systemUnderCarryOnTest.unit, expectedCarryOnUnit)
    }
    func testSUT_InitializesAirportCode() {
        XCTAssertEqual(systemUnderAirportTest.code, expectedAirportCode)
    }
   
    func testSUT_InitializesDate() {
        XCTAssertEqual(systemUnderArrivalDateTimeTest.date, expectedDate)
    }
    
    func testSUT_InitializesTime() {
        XCTAssertEqual(systemUnderArrivalDateTimeTest.time, expectedTime)
    }
    func testSUT_InitializesAirlineCode() {
        XCTAssertEqual(systemUnderAirlineTest.code, expectedAirlineCode)
    }
    
    func testSUT_InitializesAirlineName() {
        XCTAssertEqual(systemUnderAirlineTest.name, expectedAirlineName)
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.gidisUcagiControllerUnderTest = storyboard.instantiateViewController(withIdentifier: "UcuslarController") as! UcuslarController
        
        self.gidisUcagiControllerUnderTest.loadView()
        self.gidisUcagiControllerUnderTest.viewDidLoad()
//        self.donusUcagiControllerUnderTest = storyboard.instantiateViewController(withIdentifier: "DonusUcagiViewController") as! DonusUcagiViewController
//
//        self.donusUcagiControllerUnderTest.loadView()
//        self.donusUcagiControllerUnderTest.viewDidLoad()
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
