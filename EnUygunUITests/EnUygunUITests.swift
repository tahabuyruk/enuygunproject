//
//  EnUygunUITests.swift
//  EnUygunUITests
//
//  Created by Taha Buyruk on 10.05.2018.
//  Copyright © 2018 Taha Buyruk. All rights reserved.
//

import XCTest

class EnUygunUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        
        
        
        let app = XCUIApplication()
      
        
      XCTAssertTrue(app.tables/*@START_MENU_TOKEN@*/.cells.containing(.image, identifier:"Türk Hava Yolları Logo")/*[[".cells.containing(.staticText, identifier:\"Son 2 koltuk\")",".cells.containing(.staticText, identifier:\"10:55\")",".cells.containing(.staticText, identifier:\"48.99\")",".cells.containing(.staticText, identifier:\"09:55\")",".cells.containing(.staticText, identifier:\"Turkish Airlines\")",".cells.containing(.image, identifier:\"Türk Hava Yolları Logo\")"],[[[-1,5],[-1,4],[-1,3],[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.buttons["notSelected"].isHittable,"Seç butonu tıklanılamıyor")
          XCTAssertTrue(XCUIApplication().tables/*@START_MENU_TOKEN@*/.cells.containing(.image, identifier:"Türk Hava Yolları Logo")/*[[".cells.containing(.staticText, identifier:\"Son 2 koltuk\")",".cells.containing(.staticText, identifier:\"10:55\")",".cells.containing(.staticText, identifier:\"48.99\")",".cells.containing(.staticText, identifier:\"09:55\")",".cells.containing(.staticText, identifier:\"Turkish Airlines\")",".cells.containing(.image, identifier:\"Türk Hava Yolları Logo\")"],[[[-1,5],[-1,4],[-1,3],[-1,2],[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.children(matching: .staticText).matching(identifier: "Turkish Airlines").element(boundBy: 1).isHittable,"Tıklanılamıyor")
        
        
        let element = XCUIApplication().otherElements.containing(.navigationBar, identifier:"Gidiş Uçuşunu Seçin").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
          XCTAssertTrue(element.children(matching: .button).matching(identifier: " ").element(boundBy: 0).isHittable,"Önceki gün butonu tıklanılamıyor")
         XCTAssertTrue(element.children(matching: .button).matching(identifier: " ").element(boundBy: 1).isHittable,"Önceki gün butonu tıklanılamıyor")
         
        
    }
    
}
