//
//  RealmObjects.swift
//  EnUygun
//
//  Created by Taha Buyruk on 11.05.2018.
//  Copyright © 2018 Taha Buyruk. All rights reserved.
//

import Foundation
import RealmSwift

class RealmObject : Object {
    @objc dynamic var origin: String = ""
    @objc dynamic var destination: String = ""
    @objc dynamic var airlineCode: String = ""
    @objc dynamic var availableSeatCount: Int = 0
}
extension RealmObject {
    func writeToRealm(){
        try! uiRealm.write {
            uiRealm.add(self)
        }
    }
}
