import Foundation

struct Flight : Codable {

        let departure : [Departure]
        let returnField : [Return]

        enum CodingKeys: String, CodingKey {
                case departure = "departure"
                case returnField = "return"
        }
}

