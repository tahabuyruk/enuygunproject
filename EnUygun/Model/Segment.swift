import Foundation

struct Segment : Codable {

        let airlineCode : String
        let arrivalDateTime : ArrivalDateTime
        let availableSeatCount : Int
        let departureDateTime : DepartureDateTime
        let destination : String
        let origin : String

        enum CodingKeys: String, CodingKey {
                case airlineCode = "airlineCode"
                case arrivalDateTime = "arrivalDateTime"
                case availableSeatCount = "availableSeatCount"
                case departureDateTime = "departureDateTime"
                case destination = "destination"
                case origin = "origin"
        }
}
