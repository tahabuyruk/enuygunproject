import Foundation

struct Return : Codable {

        let infos : Info
        let price : Price
        let segments : Segment

        enum CodingKeys: String, CodingKey {
                case infos = "infos"
                case price = "price"
                case segments = "segments"
        }
}
