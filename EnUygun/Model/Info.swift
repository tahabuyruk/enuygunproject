import Foundation

struct Info: Codable {

        let baggageInfo : BaggageInfo
        let duration : Duration

        enum CodingKeys: String, CodingKey {
                case baggageInfo = "baggageInfo"
                case duration = "duration"
        }
}
