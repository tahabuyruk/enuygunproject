import Foundation

struct RootClass : Codable{

    let airlines : [Airline]
    let airports : [Airport]
    let flights : Flight
    
    
        enum CodingKeys: String, CodingKey {
                case airlines = "airlines"
                case airports = "airports"
                case flights = "flights"
        }
}

