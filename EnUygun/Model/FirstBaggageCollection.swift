import Foundation

struct FirstBaggageCollection : Codable {

        let allowance : Int
        let paxType : String
        let unit : String

        enum CodingKeys: String, CodingKey {
                case allowance = "allowance"
                case paxType = "paxType"
                case unit = "unit"
        }
}
