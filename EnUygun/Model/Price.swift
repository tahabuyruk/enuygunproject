import Foundation

struct Price : Codable {

        let base : Int
        let currency : String
        let service : Int
        let tax : Float
        let total : Float

        enum CodingKeys: String, CodingKey {
                case base = "base"
                case currency = "currency"
                case service = "service"
                case tax = "tax"
                case total = "total"
        }
}
