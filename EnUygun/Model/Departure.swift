import Foundation

struct Departure : Codable {

        let infos : Info
        let price : Price
        let segments : Segment

        enum CodingKeys: String, CodingKey {
                case infos = "infos"
                case price = "price"
                case segments = "segments"
        }
}
