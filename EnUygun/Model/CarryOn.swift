import Foundation

struct CarryOn : Codable {

        let allowance : Int
        let unit : String

        enum CodingKeys: String, CodingKey {
                case allowance = "allowance"
                case unit = "unit"
        }
}
