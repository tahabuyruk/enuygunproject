import Foundation

struct BaggageInfo : Codable {

        let carryOn : CarryOn
        let firstBaggageCollection : [FirstBaggageCollection]

        enum CodingKeys: String, CodingKey {
                case carryOn = "carryOn"
                case firstBaggageCollection = "firstBaggageCollection"
        }
}
