import Foundation

struct DepartureDateTime : Codable {

        let date : String
        let time : String

        enum CodingKeys: String, CodingKey {
                case date = "date"
                case time = "time"
        }
}
