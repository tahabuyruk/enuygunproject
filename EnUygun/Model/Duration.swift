import Foundation

struct Duration : Codable {

        let day : Int
        let hour : Int
        let minute : Int
        let totalMinutes : Int

        enum CodingKeys: String, CodingKey {
                case day = "day"
                case hour = "hour"
                case minute = "minute"
                case totalMinutes = "total_minutes"
        }
}
