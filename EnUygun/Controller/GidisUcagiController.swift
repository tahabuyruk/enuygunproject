//
//  UcuslarController.swift
//  EnUygun
//
//  Created by Taha Buyruk on 10.05.2018.
//  Copyright © 2018 Taha Buyruk. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class UcuslarController: UIViewController {
    
    @IBOutlet weak var navigaitonBar: UINavigationItem!
    var selectedIndexPath: NSIndexPath? = nil
    
    let smallHeight: CGFloat = 150.0
    let expandedHeight: CGFloat = 302.0
    
    var loadingView: UIView = UIView()
    //    private var dateCellExpanded: Bool = false
    
    @IBOutlet weak var dayLbl: UILabel!
    
    var dayLblText: String = ""
    var firmaIsimleri = [String]()
    var origins = [String]()
    var destinations = [String]()
    var prices = [Float]()
    var units = [String]()
    var weight = [Int]()
    var departureDateTime = [String]()
    var airlinesCode = [String]()
    var currency = [String]()
    var availableSeatCount = [Int]()
    var arrivalDateTime = [String]()
    var departureTimes = [String]()
    var landingTimes = [String]()
    var departureAirports = [String]()
    var landingAirports = [String]()
    var seatCounts = [Int]()
    var days = [Int]()
    var hours = [Int]()
    var minutes = [Int]()
    
    var customCell = CustomTableViewCell()
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var tableView: UITableView!
    
    let cellIdentifier = "customCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(activityIndicatorView)
        NotificationCenter.default.addObserver(self, selector: #selector(getJson), name: NSNotification.Name(rawValue: "load"), object: nil)
        activityIndicatorView.startAnimating()
        
        getJson()
    }
    @objc func getJson(){
        APIServices().getJson( callback: {
            orgn,destntn,prce,unts,wght,dprTime,airlinesNma,currncy,seatCount,arrvlTime,seatCnts,dayLabl,day,hour,mins   in
            
            self.origins = orgn
            self.destinations = destntn
            self.prices = prce
            self.units = unts
            self.weight = wght
            self.departureDateTime = dprTime
            self.airlinesCode = airlinesNma
            self.currency = currncy
            self.availableSeatCount = seatCount
            self.arrivalDateTime = arrvlTime
            self.seatCounts = seatCnts
            self.dayLbl.text = dayLabl
            self.days = day
            self.hours = hour
            self.minutes = mins
            self.tableView.reloadData()
            self.changeNavigationTitle()
            self.activityIndicatorView.stopAnimating()
        })
    }
    func changeNavigationTitle(){
        if CustomTableViewCell.flag{
            navigaitonBar.title = "Dönüş Uçuşunu Seçin"
        }
    }
    
}

extension UcuslarController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! CustomTableViewCell
        
        cell.configure(gelenOrigin: origins[indexPath.row], gelenDestination: destinations[indexPath.row], gelenPrice: prices[indexPath.row], gelenUnit: units[indexPath.row], gelenWeight: weight[indexPath.row], gelenDepartureDateTime: departureDateTime[indexPath.row], gelenAirlinesCode: airlinesCode[indexPath.row], gelenCurrency: currency[indexPath.row],gelenSeatCount: seatCounts[indexPath.row],gelenArrivalTime: arrivalDateTime[indexPath.row],gelenDay: days[indexPath.row],gelenHour: hours[indexPath.row],gelenMins: minutes[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return origins.count
    }
}

extension UcuslarController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //        if indexPath.row == 0 {
        //            if dateCellExpanded {
        //                dateCellExpanded = false
        //            } else {
        //                dateCellExpanded = true
        //            }
        //            tableView.beginUpdates()
        //            tableView.endUpdates()
        //        }
        
        switch selectedIndexPath {
        case nil:
            selectedIndexPath = indexPath as NSIndexPath
        default:
            if selectedIndexPath! as IndexPath == indexPath {
                selectedIndexPath = nil
            } else {
                selectedIndexPath = indexPath as NSIndexPath
            }
        }
        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        if indexPath.row == 0 {
        //            if dateCellExpanded {
        //                return 250
        //            } else {
        //                return 150
        //            }
        //        }
        //        return 150
        
        let ip = indexPath
        
        if selectedIndexPath != nil {
            if ip == selectedIndexPath! as IndexPath {
                
                return expandedHeight
            } else {
                
                return smallHeight
            }
        } else {
            return smallHeight
        }
    }
}
