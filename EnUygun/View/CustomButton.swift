//
//  CustomButton.swift
//  EnUygun
//
//  Created by Taha Buyruk on 12.05.2018.
//  Copyright © 2018 Taha Buyruk. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    
    override var isSelected: Bool  {
        
        didSet {
            if isSelected {
                self.setImage(UIImage(named: "notSelected")?.withRenderingMode(.alwaysOriginal), for: .normal)
            } else {
                self.setImage(UIImage(named: "selected")?.withRenderingMode(.alwaysOriginal), for: .normal)
            }
        }
    }
}
