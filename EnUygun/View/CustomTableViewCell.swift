//
//  CustomTableViewCell.swift
//  EnUygun
//
//  Created by Taha Buyruk on 10.05.2018.
//  Copyright © 2018 Taha Buyruk. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    let flights = RealmObject()
    
    static var flag = false
    
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    @IBOutlet weak var seatCount: UILabel!
    @IBOutlet weak var expandedDuration: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var landingAirport: UILabel!
    @IBOutlet weak var departureAirport: UILabel!
    @IBOutlet weak var landingTime: UILabel!
    @IBOutlet weak var departureTime: UILabel!
    @IBOutlet weak var currency: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var origin: UILabel!
    @IBOutlet weak var destination: UILabel!
    @IBOutlet weak var firmaIsmi: UILabel!
    @IBOutlet weak var unit: UILabel!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var departureDateTime: UILabel!
    @IBOutlet weak var firmaImage: UIImageView!
    @IBOutlet weak var firmaIsmiExpanded: UILabel!
    @IBOutlet weak var secim: CustomButton!
    //var myClass: UcuslarController = UcuslarController()
    
    
    func configure(gelenOrigin : String , gelenDestination: String , gelenPrice : Float,gelenUnit:String,gelenWeight:Int,gelenDepartureDateTime:String, gelenAirlinesCode: String,gelenCurrency: String,gelenSeatCount:Int,gelenArrivalTime : String,gelenDay: Int,gelenHour: Int,gelenMins:Int){
        
        self.flights.origin = gelenOrigin
        self.flights.destination = gelenDestination
        self.flights.airlineCode = gelenAirlinesCode
        self.flights.availableSeatCount = gelenSeatCount
        
        //   flights.writeToRealm()
        
        currency.text = gelenCurrency
        origin.text = gelenOrigin
        destination.text = gelenDestination
        price.text = "\(gelenPrice)"
        unit.text = gelenUnit
        weight.text = gelenWeight.description
        departureDateTime.text = gelenDepartureDateTime
        landingTime.text = gelenArrivalTime
        departureTime.text = gelenDepartureDateTime
        duration.text = check(gelenDay,"gün ") + check(gelenHour,"sa ") + check(gelenMins,"dakika ")
        expandedDuration.text = duration.text
        
        if availableSeatCount(closure: isSeatCountLessThan, sayi: gelenSeatCount) {
            seatCount.text = "Son \(gelenSeatCount) koltuk"
        }else {
            seatCount.isHidden = true
        }
        
        getAirlinesNameAndImage(gelenAirlinesCode)
        //   secim.isSelected = !secim.isSelected
        getDepartureAirportsName(gelenDestination)
        getArrivalAirportsName(gelenOrigin)
        
        firmaIsmiExpanded.text = firmaIsmi.text
    }
    
    func check(_ deger:Int,_ unit:String)-> String{
        
        if deger != 0 {
            
            return "\(deger) " + unit
        }else {
            
            return ""
        }
    }
    func getDepartureAirportsName(_ departure: String){
        
        switch departure{
        case  "IST":
            departureAirport.text = "Atatürk Havalimanı"
        case "SAW":
            departureAirport.text = "Sabiha Gökçen Havalimanı"
        case "ESB":
            departureAirport.text = "Esenboğa Havalimanı"
        default:
            print("Bulunamadı")
        }
    }
    func getArrivalAirportsName(_ origin: String){
        
        switch origin{
        case  "IST":
            landingAirport.text = "Atatürk Havalimanı"
        case "SAW":
            landingAirport.text = "Sabiha Gökçen Havalimanı"
        case "ESB":
            landingAirport.text = "Esenboğa Havalimanı"
        default:
            print("Bulunamadı")
        }
    }
    func getAirlinesNameAndImage(_ code: String){
        switch code {
        case "TK":
            firmaIsmi.text = "Turkish Airlines"
            firmaImage.image = UIImage(named: "Türk Hava Yolları Logo")
        case "PC":
            firmaIsmi.text = "Pegasus"
            firmaImage.image = UIImage(named: "Pegasus Logo")
        default:
            print("Bulunamadı")
        }
    }
    func availableSeatCount(closure: (Int) -> Bool, sayi: Int) -> Bool  {
        var showSeatCount =  false
        
        if closure(sayi) {
            showSeatCount = true
        }
        return showSeatCount
    }
    @IBAction func secimButton(_ sender: Any) {
        
        CustomTableViewCell.flag = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
    }
    func isSeatCountLessThan(sayi: Int) -> Bool {
        return sayi <= 2
    }
    //        func minutesToDayHoursMinutes (mins : Int) -> (String) {
    //            var minu = mins
    //            let days = minu / (60 * 24);
    //            minu -= days * (60 * 24);
    //            let hours = minu / (60);
    //            minu -= hours * (60);
    //            let minutes = minu / 60;
    //            return "\(days,hours,minutes)"
    //        }
}


