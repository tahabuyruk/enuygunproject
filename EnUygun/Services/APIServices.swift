//
//  APIServices.swift
//  EnUygun
//
//  Created by Taha Buyruk on 10.05.2018.
//  Copyright © 2018 Taha Buyruk. All rights reserved.
//

import Alamofire
import Foundation

class APIServices{
    
    var data = [RootClass]()
    let gidisVC = UcuslarController()
    
    func getJson(callback: @escaping (_ origin: [String] , _ destination: [String] , _ price: [Float], _ unit:[String],_ weight:[Int], _ departureTime: [String], _ airlinesName: [String], _ currency: [String],_ seatCount: [Int], _ arrivalTime: [String], _ seatCount: [Int],_ dayLabl: String, _ day: [Int], _ hour:[Int], _ min:[Int]) -> ()){
        
        let url = URL(string: mockyUrl)
        
        Alamofire.request(url!).responseJSON { (response) in
            
            let result = response.data
            do {
                self.data  = [try JSONDecoder().decode(RootClass.self, from: result!)]
                
                for data in self.data {
                    
                    if !CustomTableViewCell.flag {
                        
                        let departure = data.flights.departure
                        
                        for dep in departure {
                            self.gidisVC.currency.append(dep.price.currency)
                            self.gidisVC.airlinesCode.append(dep.segments.airlineCode)
                            self.gidisVC.origins.append(dep.segments.origin)
                            self.gidisVC.destinations.append(dep.segments.destination)
                            self.gidisVC.prices.append(Float(dep.price.total))
                            self.gidisVC.units.append(dep.infos.baggageInfo.carryOn.unit)
                            self.gidisVC.weight.append(dep.infos.baggageInfo.carryOn.allowance)
                            self.gidisVC.departureDateTime.append(dep.segments.departureDateTime.time)
                            self.gidisVC.availableSeatCount.append(dep.segments.availableSeatCount)
                            self.gidisVC.arrivalDateTime.append(dep.segments.arrivalDateTime.time)
                            self.gidisVC.seatCounts.append(dep.segments.availableSeatCount)
                            self.gidisVC.dayLblText = dep.segments.departureDateTime.date.description
                            self.gidisVC.days.append(dep.infos.duration.day)
                            self.gidisVC.hours.append(dep.infos.duration.hour)
                            self.gidisVC.minutes.append(dep.infos.duration.minute)
                        }
                    } else {
                        let returnFlights = data.flights.returnField
                        
                        for rFlights in returnFlights {
                            self.gidisVC.currency.append(rFlights.price.currency)
                            self.gidisVC.airlinesCode.append(rFlights.segments.airlineCode)
                            self.gidisVC.origins.append(rFlights.segments.origin)
                            self.gidisVC.destinations.append(rFlights.segments.destination)
                            self.gidisVC.prices.append(Float(rFlights.price.total))
                            self.gidisVC.units.append(rFlights.infos.baggageInfo.carryOn.unit)
                            self.gidisVC.weight.append(rFlights.infos.baggageInfo.carryOn.allowance)
                            self.gidisVC.departureDateTime.append(rFlights.segments.departureDateTime.time)
                            self.gidisVC.availableSeatCount.append(rFlights.segments.availableSeatCount)
                            self.gidisVC.arrivalDateTime.append(rFlights.segments.arrivalDateTime.time)
                            self.gidisVC.seatCounts.append(rFlights.segments.availableSeatCount)
                            self.gidisVC.dayLblText = rFlights.segments.departureDateTime.date.description
                            self.gidisVC.days.append(rFlights.infos.duration.day)
                            self.gidisVC.hours.append(rFlights.infos.duration.hour)
                            self.gidisVC.minutes.append(rFlights.infos.duration.minute)
                        }
                    }
                }
                callback(self.gidisVC.origins,self.gidisVC.destinations,self.gidisVC.prices,self.gidisVC.units,self.gidisVC.weight,self.gidisVC.departureDateTime,self.gidisVC.airlinesCode,self.gidisVC.currency,self.gidisVC.availableSeatCount,self.gidisVC.arrivalDateTime,self.gidisVC.seatCounts,self.gidisVC.dayLblText,self.gidisVC.days,self.gidisVC.hours,self.gidisVC.minutes)
                
            }catch {
                print(error)
            }
        }
    }
}


